#ifndef ZAD5_BOOK_H
#define ZAD5_BOOK_H
#include <fstream>
#include <iostream>


class Book {
private:
    std::ifstream file;
    char* buffer;
    std::streamoff length;
    int position=0;

public:
    void load_file();
    void reciteAllForward();
    void reciteAllBackward();
    void wordForward();
    void wordBackward();
    void verseForward();
    void verseBackward();
    void closeFile();
};


#endif
