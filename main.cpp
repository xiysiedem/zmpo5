#include <iostream>
#include "Book.h"
#include "Calc.h"

using namespace std;

void showmenu() {
    cout <<
    " 0-wyświetl menu \n 1- recytuj caly tekst od przodu \n 2 - recytuj caly tekst od tylu \n 3 - nastepne slowo"
            " \n 4- poprzednie slowo \n 5- nastepny wers \n 6- poprzedni wers \n 7 - wyjscie\n";
}

int main() {
    Book book;
    book.load_file();

    bool menu = true;
    int option = 0;
    showmenu();

    while (menu) {
        cin >> option;
        switch (option) {
            case 0:
                showmenu();
                break;
            case 1:
                book.reciteAllForward();
                break;
            case 2:
                book.reciteAllBackward();
                break;
            case 3:
                book.wordForward();
                break;
            case 4:
                book.wordBackward();
                break;
            case 5:
                book.verseForward();
                break;
            case 6:
                book.verseBackward();
                break;
            case 7:
                menu = false;
                book.closeFile();
                break;
            default: cout<<"taka opcja nie istnieje - wybierz 0 aby zoabczyc dostepne opcje";
                break;
        }
        cout << endl;
    }
    return 0;
}