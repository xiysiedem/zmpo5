#include <iostream>
#include "Book.h"

using namespace std;

void Book::load_file() {
    file.open("/home/xiysiedem/ClionProjects/ZMPO/zad5/text");
    if (file) {
        file.seekg(0, file.end);
        length = file.tellg();
        file.seekg(0, file.beg);

        buffer = new char[length];

        file.read(buffer, length);

    } else {
        cout << "Nie udalo otworzyc pliku";
    }
}

void Book::reciteAllForward() {
    position = 0;
    while (position < length) {
        cout << buffer[position];
        position++;
    }
}

void Book::reciteAllBackward() {
    position = (int) length - 2;
    while (position > 0) {
        wordBackward();
        cout << " ";
    }

}

void Book::wordForward() {
    if ((int) buffer[position] == 32) {
        position++;
    }
    while ((int) buffer[position] != 32 && position < length) {
        cout << buffer[position];
        position++;
    }
}

void::Book::wordBackward() {
    if ((int) buffer[position] == 32) {
        position--;
    }
    while ((int) buffer[position] != 32 && position > 0) {
        position--;
    }
    int tmp = position;
    wordForward();
    position = tmp;
}

void Book::verseForward() {
    if ((int) buffer[position] == 10) {
        position++;
    }
    while ((int) buffer[position] != 10 && position < length) {
        cout << buffer[position];
        position++;
    }
}

void Book::verseBackward() {
    if ((int) buffer[position] == 10) {
        position--;
    }
    while ((int) buffer[position] != 10 && position > 0) {
        position--;
    }
    int tmp = position;
    verseForward();
    position = tmp;

}

void Book::closeFile() {
    file.close();
    delete[] buffer;
}